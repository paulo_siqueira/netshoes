import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 *
 * @author Paulo Henrique
 */
public class Ex11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double num = 1.999;
        BigDecimal x = BigDecimal.valueOf(num);     
        DecimalFormat formatter = new DecimalFormat(".##");
        formatter.setRoundingMode(RoundingMode.DOWN);
        System.out.print(formatter.format(x)); 

    }

}
