/**
 *
 * @author Paulo Henrique
 */
public class Father extends Man {

    public String isOld(String strAge) {

        try {
            int age = Integer.parseInt(strAge);
            return super.isOld(age) ? "idoso" : "jovem";
        } catch (NumberFormatException ex) {
            throw new NumberFormatException("o parâmetro passado não é um número válido");
        }

    }
}
