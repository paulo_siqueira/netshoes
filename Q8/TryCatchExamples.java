/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication9;

/**
 *
 * @author Paulo Henrique
 */
public class TryCatchExamples {

    public static void main(String[] args) {
        try {
            String str=null;
            str.toUpperCase();
        } catch (NullPointerException e1) {
            System.out.println("deu nullPointer");
            fazAlgumaCoisa();
        }finally{
            System.out.println("Sempre vai ser executado");
        }
        
    }
    
    public static void fazAlgumaCoisa(){
        System.out.println("Faz alguma coisa");
    }
}
